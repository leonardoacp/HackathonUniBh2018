﻿using System;

namespace SimplexCardPortal.ViewModel.OperatorBranch
{
    public class OperatorBranchViewModel
    {
        public Guid OperatorBranchId { get; set; }
        public Guid OperatorId { get; set; }
        public string OperatorName { get; set; }
        public string UserName { get; set; }
        public string Url { get; set; }
        public bool Status { get; set; }
    }
}
