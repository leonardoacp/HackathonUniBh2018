﻿using System;

namespace SimplexCardPortal.ViewModel.OperatorBranch
{
    public class EditOperatorBranchViewModel
    {
        public Guid OperatorBranchId { get; set; }
        public string OperatorName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
