﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SimplexCardPortal.ViewModel.Settings
{
    public class OrdersViewModel
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Escala de trabalho")]
        public int WorkSchedule { get; set; }

        public IEnumerable<SelectListItem> WorkSchedules { get; set; } 
    }
}
