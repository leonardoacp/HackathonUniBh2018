﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SimplexCardPortal.ViewModel.Order
{
    public class NewOrder
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Operadora")]
        public Guid OperatorBranchId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Data do saldo na Operadora")]
        public Guid? OperatorDataLogId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Arquivo")]
        public IFormFile File { get; set; } 

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data da carga")]
        public DateTime? ChargeDate { get; set; }

        [Display(Name = "Observação")]
        public string Note { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [Display(Name = "Classificaçāo")]
        public int Type {get;set;}


        public IEnumerable<SelectListItem> Operators { get; set; }
        public IEnumerable<SelectListItem> Types { get; set; }
    }
}
