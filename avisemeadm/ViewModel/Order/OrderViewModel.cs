﻿using System;


namespace SimplexCardPortal.ViewModel.Order
{
    public class OrderViewModel
    {
        public Guid OrderId { get; set; }
        public int Number { get; set; }
        public string OperatorName { get; set; }
        public string OperatorUrl { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int Type { get; set; }
        public string TypeText { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public string StatusClass { get; set; }
    }
}



