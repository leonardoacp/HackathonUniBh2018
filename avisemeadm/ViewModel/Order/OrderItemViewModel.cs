﻿using System;
using System.Collections.Generic;

namespace SimplexCardPortal.ViewModel.Order
{
    public class OrderItemViewModel
    {
        public Guid OrderId { get; set; }
        public int Number { get; set; }
        public string OrderStatus { get; set; }
        public int OrderStatusValue { get; set; }
        public string OperatorName { get; set; }
        public string TotalEconomy { get; set; }
        public string TotalRequested { get; set; }
        public int TotalCardsRequested { get; set; }
        public int TotalCardsLoaded { get; set; }
        public string TotalOrderWithEconomy { get; set; }
        public int TotalActive { get; set; }
        public int TotalPending { get; set; }
        public int TotalDisabled { get; set; }
        public string ChargeDate { get; set; }
        public string BalanceDate { get; set; }
        public string CreatedOn { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public List<OrderItems> OrderItems { get; set; }
    }

    public class OrderItems {

        public Guid OrderItemId { get; set; }
        public string Identifier { get; set; }
        public string CardNumber { get; set; }
        public string Name { get; set; }
        public string DailyValue { get; set; }
        public int WorkDays { get; set; }
        public string Balance { get; set; }
        public int? WorkDaysWithEconomy { get; set; }
        public string Economy { get; set; }
        public string Requested { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public bool Active { get; set; }
        public string StatusOptionText { get; set; }
        public string StatusText { get; set; }
        public string StatusClass { get; set; }
        public string EconomyPercentage { get; set; }
    }
}



    