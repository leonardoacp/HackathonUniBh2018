﻿using System.ComponentModel.DataAnnotations;

namespace SimplexCardPortal.ViewModel.Account
{
    public class LoginViewModel
    {

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        [Display(Name = "Permanecer conectado")]
        public bool RememberMe { get; set; }
    }
}
