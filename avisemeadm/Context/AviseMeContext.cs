using System.IO;
using avisemeadm.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace avisemeadm.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<Context.AvisemeContext>
    {
        public AvisemeContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<Context.AvisemeContext>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            builder.UseSqlServer(connectionString);

            return new AvisemeContext(builder.Options);
        }
    }

    public class AvisemeContext : IdentityDbContext
    {
        public AvisemeContext(DbContextOptions<AvisemeContext> options)
            : base(options)
        {
        }

        public AvisemeContext()
        {
        }

        public DbSet<Model.ProfessionalLocation> ProfessionalLocations { get; set; }
        public DbSet<Model.EventLocation> EventLocations { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.AddConfiguration(new Mapping.ProfessionalLocationMap());
            builder.AddConfiguration(new Mapping.EventLocationMap());

            base.OnModelCreating(builder);
        }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			// get the configuration from the app settings
			var config = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();

			// define the database to use
			optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
		}
    }
}
