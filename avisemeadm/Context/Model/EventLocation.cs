using System;

namespace avisemeadm.Context.Model
{
    public class EventLocation
    {
        public Guid EventLocationId{get;set;}
        public string Name{get;set;}
        public string ProfessionalType {get;set;}
        public string Longitude{get;set;}
        public string Latitude{get;set;}
        public DateTime LastExecution {get;set;}
        public string Telephone {get;set;}
        public string Description{get;set;}
    }
}