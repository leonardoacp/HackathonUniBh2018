using System;

namespace avisemeadm.Context.Model
{
    public class ProfessionalLocation
    {
        public Guid ProfessionalLocationId{get;set;}
        public string Name{get;set;}
        public string ProfessionalType {get;set;}
        public string Occupation{get;set;}
        public string Longitude{get;set;}
        public string Latitude{get;set;}
        public DateTime LastExecution {get;set;}
        public string Telephone {get;set;}
    }
}