using avisemeadm.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace avisemeadm.Context.Mapping
{
    public class ProfessionalLocationMap: EntityTypeConfiguration<Model.ProfessionalLocation>
    {
        public override void Map(EntityTypeBuilder<Model.ProfessionalLocation> builder)
        {
            // Primary Key
            builder.HasKey(t => t.ProfessionalLocationId);


            // Table & Column Mappings
            builder.ToTable("ProfessionalLocation");
            builder.Property(t => t.ProfessionalLocationId).HasColumnName("ProfessionalLocationId");
            builder.Property(t => t.Name).HasColumnName("Name");
            builder.Property(t => t.ProfessionalType).HasColumnName("ProfessionalType");
            builder.Property(t => t.Occupation).HasColumnName("Occupation");
            builder.Property(t => t.Longitude).HasColumnName("Longitude");
            builder.Property(t => t.Latitude).HasColumnName("Latitude");
            builder.Property(t => t.LastExecution).HasColumnName("LastExecution");
            builder.Property(t => t.Telephone).HasColumnName("Telephone");

        }
    }
}