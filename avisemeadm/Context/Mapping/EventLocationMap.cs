using avisemeadm.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace avisemeadm.Context.Mapping
{
    public class EventLocationMap: EntityTypeConfiguration<Model.EventLocation>
    {
        public override void Map(EntityTypeBuilder<Model.EventLocation> builder)
        {
            // Primary Key
            builder.HasKey(t => t.EventLocationId);


            // Table & Column Mappings
            builder.ToTable("EventLocation");
            builder.Property(t => t.EventLocationId).HasColumnName("EventLocationId");
            builder.Property(t => t.Name).HasColumnName("Name");
            builder.Property(t => t.ProfessionalType).HasColumnName("ProfessionalType");
            builder.Property(t => t.Longitude).HasColumnName("Longitude");
            builder.Property(t => t.Latitude).HasColumnName("Latitude");
            builder.Property(t => t.LastExecution).HasColumnName("LastExecution");
            builder.Property(t => t.Telephone).HasColumnName("Telephone");
            builder.Property(t => t.Description).HasColumnName("Description");
        }
    }
}