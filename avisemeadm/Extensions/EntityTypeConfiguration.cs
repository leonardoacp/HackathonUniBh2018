using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace avisemeadm.Extensions
{
    public abstract class EntityTypeConfiguration<TEntity> where TEntity : class
    {
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }
}