﻿using System;
namespace SimplexCardPortal.Enum
{
    public class Branch
    {
        public enum WorkSchedule
        {
            SegundaASexta = 0,
            SegundaASabado = 1,
            TodosOsDias = 2,
            SegundaAQuinta = 3
        }
    }
}
