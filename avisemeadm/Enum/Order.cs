﻿using System.ComponentModel;

namespace SimplexCardPortal.Enum
{
    public class Order
    {
        public enum Status
        {
            [Description("Pendente")]
            Pendente = 0,

            [Description("Aprovado")]
            Aprovado = 1,

            [Description("Desativado")]
            Desativado = 2
        }

        public enum Type
        {
            [Description("Mensal")]
            Mensal = 0,

            [Description("Semanal")]
            Semanal = 1,

            [Description("Inconsistência")]
            Inconsistencia = 2
        }
    }
}
