using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimplexCardPortal.Models;
using SimplexCardPortal.Models.Authorization;

namespace SimplexCardPortal.Controllers
{
    public class EventLocationController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(avisemeadm.Context.Model.EventLocation eventLocation)
        {
            using (var dbContext = new avisemeadm.Context.AvisemeContext())
            {
                eventLocation.EventLocationId = Guid.NewGuid();
                eventLocation.LastExecution = new DateTime(2018,06,10);

                dbContext.EventLocations.Add(eventLocation);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpPost]
        public IActionResult FilterInstitution(List<string> institution){

        List<avisemeadm.Context.Model.EventLocation> positions;

        if(institution.Contains("0")){
            institution = null;
        }

        using(var dbContext = new avisemeadm.Context.AvisemeContext()){

                positions = dbContext.EventLocations.Where(a => institution.Contains(a.ProfessionalType) || institution ==  null)
                .OrderByDescending(a => a.LastExecution)
                .GroupBy(a => new { a.ProfessionalType, a.Name }).Select(a => a.FirstOrDefault()).ToList();
        }

        return PartialView("_Map", positions);

        }
    }
}
