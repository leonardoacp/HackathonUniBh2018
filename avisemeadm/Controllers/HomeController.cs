﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimplexCardPortal.Models;
using SimplexCardPortal.Models.Authorization;

namespace SimplexCardPortal.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        [HttpPost]
        public IActionResult FilterInstitution(List<string> institution){

        List<avisemeadm.Context.Model.ProfessionalLocation> positions;

        if(institution.Contains("0")){
            institution = null;
        }

        using(var dbContext = new avisemeadm.Context.AvisemeContext()){

                positions = dbContext.ProfessionalLocations.Where(a => institution.Contains(a.ProfessionalType) || institution ==  null)
                .OrderByDescending(a => a.LastExecution)
                .GroupBy(a => new { a.ProfessionalType, a.Name }).Select(a => a.FirstOrDefault()).ToList();
        }

        return PartialView("_Map", positions);

        }
    }
}
