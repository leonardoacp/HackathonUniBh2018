﻿using System;
namespace SimplexCardPortal.Models
{
    public static class Helper
    {
        public static DateTime Brazilia(this DateTime datetime)
        {
            return TimeZoneInfo.ConvertTime(datetime, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
        }
    }
}
