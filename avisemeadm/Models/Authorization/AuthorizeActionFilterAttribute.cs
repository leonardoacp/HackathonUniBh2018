﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SimplexCardPortal.Models.Authorization
{
    public class AuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;


        public AuthorizationFilter()
        {
            _httpContextAccessor = new HttpContextAccessor(); 
        }
        
        public void OnAuthorization(AuthorizationFilterContext context)
        {

        }
    }
}
