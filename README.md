Documentação HACKATON


Integrantes: Henrique Corradi Bernardes			RA: 11313970 <br />
	     Leonardo de Almeida Calcagno Peixoto       RA: 11220937 <br />


![alt text](http://avisemeadm.azurewebsites.net/images/logo.png)

Produto: Aviseme ADM <br />
Url: http://avisemeadm.azurewebsites.net/login <br />
Usuario: admin@aviseme.com.br <br />
Senha: admin <br />

Descrição dos Processos

Etapa 1:

O profissional com posse de um celular concedido pela prefeitura, ira mandar de forma automática os dados de latitude e 
longitude através do GPS. (Atualmente simulamos a entrada desses dados)

Etapa 2:
Com esse dados, sera possível projetar no mapa a posição exata de cada agente, para que caso haja algum problema seja possível
realizar a localização do mesmo.


Após alguns levantamentos de ideias, foi decidido desenvolver uma aplicação web via GPS capaz de informar aos profissionais 
da área municipal possíveis ocorrências (assaltos, furtos, acidentes de trânsito, entre outros) que tenham acontecido.
Ao mesmo tempo, é possível que qualquer pessoa informe na aplicação sobre os acontecimentos relevantes ocorridos ao seu redor. 
Para que isso seja possível, é necessário que a mesma já esteja cadastrada na aplicação.
Para o desenvolvimento dessa aplicação, foi utilizada uma API do Google Maps para que fosse possível utilizar as marcações no mapa. 
O código foi escrito no editor de texto Visual Studio Code e o banco de dados utilizado foi o SQLServer(Docker).
No mapa plotado, os pontos foram coloridos para que haja facilidade por parte do usuário em identificar sua informação desejada. 
Haverá uma legenda logo acima do mapa para descrever o que cada cor representa. 
A aplicação foi testada com 10.117 dados.


Diferencial: Com as informações que o cidadão ira passar atraves de avisos informando assim a descrição do aviso e localização do ocorrido,
será possível auxiliar o operador que esta remanejando um ou mais profissionais para uma ocorrencia. O sistema tratara isso de forma automatica,
mostrando ao mesmo uma projeção de quantos profissionais é o ideal remanejar levando em conta a quantidade de ocorrencias abertas pelos usuários.


Etapas de instalação:<br />

Softwares utilizados:<br />
Visual Studio Code<br />
SQL Server<br />

Banco de Produção<br />
Server=tcp:hackathonunibh2018.database.windows.net,1433<br />
Banco: hackathonunibh2018<br />
Usuário: hack<br />
Senha: teste01*9<br />


1 - O projeto esta na seguinte pasta: https://gitlab.com/leonardoacp/HackathonUniBh2018/tree/master/avisemeadm<br />
2 - Faça o download do .net Core: <br />
https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.300-windows-x64-installer<br />
https://www.microsoft.com/net/download/thank-you/dotnet-runtime-2.1.0-windows-hosting-bundle-installer<br />

3 - Caso queira executar o projeto com o banco no ambiente local(SQL Server), crie um banco com o nome hackathonunibh2018, caso contrário vá ate o passo 6<br />
4 - O ambiente local deve estar configurado como esta descrito no arquivo appsettings.json: https://gitlab.com/leonardoacp/HackathonUniBh2018/blob/master/avisemeadm/appsettings.json <br />
5 - Assim que o banco esta criado, abra o terminal/linha de comando e acesse a pasta descrita no item 1, execute o comando: dotnet ef database update e depois: dotnet run<br />
6 - Para testar o projeto no banco de produção basta fazer o passo 7, ele é o melhor caso para testar, pois não precisa criar o ambiente de teste(banco)<br />
7 - Abra o terminal/linha de comando e acesse a pasta descrita no item 1, execute o comando: dotnet run<br />




